﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*'!!!*Q(C=\&gt;5^4C."%)&lt;B&lt;R%"K7_!L)E176X"6ZC!(.56H*)A_1!LJ!I).P5*E(S&amp;3AF^"6^B?,N&gt;D$&gt;9L:-&amp;I25^N*HZ_O_B:TR)P6R*FZL/F?'PJ&lt;7X4OV8PTAGQ`PFM7GI^O(5=;L,;2YRT50HLD8F0/RU-JT;J^/S@VT`D0^-U8@Z,P^X?4D\*@DQ\]%0N2=2.;F"&gt;;KJ,4MF?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:(XAVTE)B=ZJ'4R:+&amp;EUG3#J$-5*2_**`%EHM4$J2*0YEE]C3@RU%7**`%EHM34?"CGR*.Y%E`C34R-V38:$X)]C9@J&amp;8A#4_!*0)'(*26Y!E#Q7$"R-!E-"9X"3?!*0)'(5Q7?Q".Y!E`AI6G"*`!%HM!4?"D3&gt;S7[JBXE?*B'DM@R/"\(YXC97I\(]4A?R_.Y7%[/R`%Y#'&gt;":X))=A9Z(:Q,R_.Y_#8(YXA=D_.R0$4V/_2^:ZKG(?2Y$)`B-4S'R`!QB1S0Y4%]BM@Q-+U-D_%R0)&lt;(],#5$)`B-4Q'R&amp;C5Z76-:AQU/BG"Y?'H0SX7\V*UC@6$KI&gt;8^6#K(D&lt;61[2[/&amp;1X885T64&gt;*N@GK466NFGI46(_=#KX#K":2$7Y&gt;&gt;?"T4^V2N^1.&gt;5U&gt;K3PKELJI1`^RR]0BI0V_L^VOJ_VWK]VGI`6[L8%=N6KNN&amp;QON6AMZN@!.=@]1DC_FRYY@\[^P`NV]`1;NY`DY]X0]38P\VL_#@_@P]#\52@[@1XW[!U`3&amp;`U!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="CMD_Loop.vi" Type="VI" URL="../CMD_Loop.vi"/>
	<Item Name="Global_CMD_CMD_Data In.vi" Type="VI" URL="../Global_CMD_CMD_Data In.vi"/>
	<Item Name="Global_CMD_Data Out.vi" Type="VI" URL="../Global_CMD_Data Out.vi"/>
	<Item Name="Cluster_CMD_CMD_Data.ctl" Type="VI" URL="../Cluster_CMD_CMD_Data.ctl"/>
	<Item Name="ENUM_CMD_Action.ctl" Type="VI" URL="../ENUM_CMD_Action.ctl"/>
	<Item Name="ENUM_CMD_State.ctl" Type="VI" URL="../ENUM_CMD_State.ctl"/>
</Library>

﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)]!!!*Q(C=\&gt;5RDN.!&amp;-&lt;R$U2"GW),/B2J4`"KOL35PM+\1NIN1UHZC9IW(87O]&amp;I+CFQB6`$_04T-3IMW$3#%&gt;JR*H-]T&lt;XZL/VZJN.@3+]X8WPMHWX+=0P@HAT[PR];Y(W08L@@H0N;PRX6_(BQV(]_@@TV`8OM`\&lt;`3^.S?W``&gt;\K\_#0\Y\_#&amp;FA=2P?CG*TWU,$M8?:%8?:%8?:'&lt;X/1G.\H*4:\E3:\E3:\E32\E12\E12\E14YW=J',8/31CM7,B9KC29&amp;C-"16&lt;Y7H]"3?QM.8&amp;:\#5XA+4_&amp;BC!J0Y3E]B;@Q-%W&amp;J`!5HM*4?#AV*$5W=DS&amp;B`)S(O-R(O-R(J;5]2C!7=Q5.E6AS"QU/]:D0-&lt;$LIT(?)T(?)S(QT)?YT%?YT%?JISTYK&amp;:.H)]F&amp;(C34S**`%E(EIL]33?R*.Y%A`,+@%EHA32,*A5B["E5D)A_:*Y%A]@3DS**`%EHM4$I8'&amp;=JS:2&lt;.MZ(A#4_!*0)%H]&amp;"#A3@Q"*\!%XAIK]!4?!*0Y!E],+8!%XA#4Q!*&amp;G6Z"=7#C='A)!A]P-&lt;&gt;%O-K?5BC&lt;&amp;,@P/K&lt;5HWTK7]C^=WBPODKC[G_3/K4LT[J[J/F0AHK0U[.6G05C[AH,Q.VY@V-0^'0^!.^4Z`I/`K7PFGG`O;"F]N&amp;Z`.:J^.*R_.2B]."_`V?UT2JN^NJO^VKM^GMDY'X&lt;/M$Y@NT[9\^4`;XTW_GGY_X^I&gt;XU]W87X^&gt;]L`Q``E@?$&lt;KJ2[OQ4G["U@M?G)!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../../ACT_Loop.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../../Global_ACT_Data Out.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../../ENUM_ACT_State.ctl"/>
	<Item Name="ENUM_CMD_Cone_Stabber_Mode select.ctl" Type="VI" URL="../ENUM_CMD_Cone_Stabber_Mode select.ctl"/>
</Library>

﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;5R&lt;B."&amp;-&lt;R$R1*7N]!G2/A&gt;Q5,4O#+`L5J+&amp;QB5@I+\QK_AHN8XQF!FG`A,E*+%`Y\0'Q#)GY!):1&gt;DR.`M`0GF^HV2BL(=_F+&gt;Z?/X90('.@I6.S&gt;X]9+J`'P\@T;^;R\Y^+X7LI``P.E`7K]F`ZB`E0_#Y=?D]@D`TY_80Q3`0(PQ2..$S+[[560?GB;^M\E*D?ZS5VO]C)P]C)P]C)P]C20]C20]C20]C!0]C!0]C!0]N()23ZSE5-SCZO&amp;4&amp;&amp;4Q*Q-2?&lt;.?)T(?)S(DT)?YT%?YT%?4J(R')`R')`R-%X'9TT'9TT'1[EB]7DE?)S(]CI]B;@Q&amp;*\#QZ)K0!7A7+QI8"3"I7+Q_+8Q&amp;*\#Q[]K0)7H]"3?QM/Q#E`B+4S&amp;J`!Q:?R+$=X5S0&amp;12IEH]33?R*.Y++X%EXA34_**0#SHR*.Y%E3S9&amp;)=AJ**S1H*B]34?0CBR*.Y%E`C34Q-D3O59W=GT&gt;4)]13?Q".Y!E`AI91#4_!*0)%H]&amp;"7A3@Q"*\!%XB93I%H]!3?!")MSP)+CA54AZ/#)0$Q'H&gt;,D+N51R+D38XTKG^+^=WGPIH5.Y@[IKMPJPICK4&gt;@P;HKT6*PAPK05[06'05C[MH4C4LSPK&gt;P[2P[GL[C,_E,_JQ_G[&lt;_ZB/0R[0W_\WWW[UWGYX7[\67KZ77S[57CY8G]\FGM^HJ-@##&gt;HIAH*^,HV_`?H&lt;T^PJQ]_&lt;&gt;Y@&lt;4_]0NRZ?(P`4`_2^Y.OKJPF_$0@I#$GG3!!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="Outputs FX Read.vi" Type="VI" URL="../Outputs FX Read.vi"/>
	<Item Name="Outputs SW Set.vi" Type="VI" URL="../Outputs SW Set.vi"/>
	<Item Name="Config CanCoder Set.vi" Type="VI" URL="../Config CanCoder Set.vi"/>
	<Item Name="Config FX Extract.vi" Type="VI" URL="../Config FX Extract.vi"/>
	<Item Name="Config HW Extract.vi" Type="VI" URL="../Config HW Extract.vi"/>
	<Item Name="Config HW Set.vi" Type="VI" URL="../Config HW Set.vi"/>
	<Item Name="Config FX Set.vi" Type="VI" URL="../Config FX Set.vi"/>
	<Item Name="Manual Override.vi" Type="VI" URL="../Manual Override.vi"/>
	<Item Name="Outputs HW Read.vi" Type="VI" URL="../Outputs HW Read.vi"/>
</Library>

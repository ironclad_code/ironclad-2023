﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)N!!!*Q(C=\&gt;7R5=.!%)8B:];")W&lt;=!?-7NA7XY";W";=I=QP&lt;AFNQ2,YN/+!"J94CP`/;A1(D""C'Y=4*UDP&gt;X??4,+2?&amp;N*=U\7S_,3=W[@4S&lt;39TLPJ@8P&lt;PTH`K0W58'[`U,_GP&gt;2_W8_F[,`]F\^&gt;\K`_#,\^&gt;T"4?R&amp;2ERJ5JZL;N&amp;/3*XG3*XG3*XG1"XG1"XG1"\G4/\G4/\G4/\G2'\G2'\G2'XH@S%5O=J&amp;$3C:0*EI'41:),I;C:*&gt;Y%E`C34S=+P%EHM34?")0FSDR**\%EXA3$^W5?"*0YEE]C9?BOC4\2IYH]4#]!E`A#4S"*`!QJ1*0!!AG#Q9/"I'BI$%Y#$S"*`"QK-!4?!*0Y!E].#PQ"*\!%XA#$VX[KE48N)U=$]0)]4A?R_.Y(!^$S`%Y(M@D?"Q0U]HR/"Y(Y5TI$!Z"4C@H!O@%]4A?0O2Y()`D=4S/B[:_B\SP4./UD2S0Y4%]BM@Q'"['E/%R0)&lt;(]"A?BJ8B-4S'R`!9(K;3Y4%]BM?!'*-SP9T"D)\'25:A?0DL4YPVOR2&gt;9HW4[O&amp;60:3KBUXV%+E?$N6.6^V-V5V3,&lt;ZK567,J6I%V:&gt;4I659V33KTOV#D?S0V!.V4^V2N^1.&gt;5V&gt;5:?N[R&gt;@/)[DDM?D$I?$^PO^&gt;LO&gt;NNON.JO.VOOV6KO6FMPFSWPADOXFB8"[,^VT0.Q_01TT94&lt;-(W@^=RRG,@_"`]_`Y.WI'\W?AT6["BH2.3=!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Outputs HW Read.vi" Type="VI" URL="../Outputs HW Read.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="Outputs FX Read.vi" Type="VI" URL="../Outputs FX Read.vi"/>
	<Item Name="Outputs SW Set.vi" Type="VI" URL="../Outputs SW Set.vi"/>
	<Item Name="Config CanCoder Set.vi" Type="VI" URL="../Config CanCoder Set.vi"/>
	<Item Name="Config FX Extract.vi" Type="VI" URL="../Config FX Extract.vi"/>
	<Item Name="Config HW Extract.vi" Type="VI" URL="../Config HW Extract.vi"/>
	<Item Name="Config HW Set.vi" Type="VI" URL="../Config HW Set.vi"/>
	<Item Name="Config FX Set.vi" Type="VI" URL="../Config FX Set.vi"/>
	<Item Name="Manual Override.vi" Type="VI" URL="../Manual Override.vi"/>
</Library>
